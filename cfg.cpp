#include<iostream>
#include<vector>
using namespace std;
//each node contains following field
struct node
{
	vector<int> successor;
	vector<int> predecessor;
	int s[4];
	int d[4];
	int D[4];
};

void xor1(int a[],int b[],int ans[]);//to perform xor operation
void decbin(int s[],int n);//convert decimal to binary value
void initialize_signature(struct node v[],int n);//to give unique signature to each node
void compute_difference_signature(struct node v[],int n);//computes the signature difference
void copy(int source[],int dest[],int n);//to copy array
void output(int a[],int n);
	
int main()
{
	cout<<"enter no of nodes"<<endl;
	int n;
	cin>>n;
	struct node v[n+1];
	initialize_signature(v,n);
	cout<<"number of edges"<<endl;
	int e;
	cin>>e;
	for(int i=0;i<e;i++)//for each edge predescer and successor list of respective node gets updated
	{
		cout<<"enter sorce"<<endl;
		int source;
		cin>>source;
		int dest;
		cout<<"enter dest"<<endl;
		cin>>dest;
		v[dest].predecessor.push_back(source);
		v[source].successor.push_back(dest);
	}
	compute_difference_signature(v,n);
	for(int i=1;i<=n;i++)
	{
		cout<<"for node "<<i<<endl;
		cout<<"s"<<"\t";
		output(v[i].s,4);
		cout<<"d"<<"\t";
		output(v[i].d,4);
		cout<<"D"<<"\t";
		output(v[i].D,4);
	}
		return 0;
}

void xor1(int a[],int b[],int ans[])
{	
	
	for(int i=0;i<4;i++)
	{
		if(a[i]==b[i])
			ans[i]=0;
		else
			ans[i]=1;
	}	

};
int count=1;//global variable to keep track of numberng the nodes
void decbin(int s[],int n)//it converts value of count to binary value
{
	
	int k=0;
	int div=count;
	while(div)
	{	
		s[n-k-1]=div%2;
		k++;
		div=div/2;
	}	
	for(int l=0;l<n-k;l++)//insert zero at left
	{
	s[l]=0;		
	}
	count++;//after the operation increment the value of count
};



void copy(int source[],int dest[],int n)
{
	for(int i=0;i<n;i++)
		dest[i]=source[i];
};	

void output(int a[],int n)
{
	for(int i=0;i<n;i++) 
		cout<<a[i]<<" ";
	cout<<endl;	
};

void initialize_signature(struct node v[],int n)
{
	for(int i=1;i<=n;i++)
	{
			decbin(v[i].s,4);
	}
};

void compute_difference_signature(struct node v[],int n)
{
	for(int i=1;i<=n;i++)
	{
		if((v[i].predecessor).size()==0)//if v[i] is the first node
			copy(v[i].s,v[i].d,4);
		else if((v[i].predecessor).size()==1)//if v[i] has one predecessor
		{	
			int ans[4];
			xor1(v[v[i].predecessor[0]].s,v[i].s,ans);
			copy(ans,v[i].d,4);
		}
		else//if v[i] has many predecessor
		{
			int ans[4];
			int zero[4]={0,0,0,0};
			xor1(v[v[i].predecessor[0]].s,v[i].s,ans);
			copy(ans,v[i].d,4);
			copy(zero,v[v[i].predecessor[0]].D,4);
			for(int j=1;j<=(v[i].predecessor).size();j++)//loop through all the predecessor of v[i]
			{
				int ans[4];
				xor1(v[v[i].predecessor[0]].s,v[v[i].predecessor[j]].s,ans);//calculate value of D 				
				copy(ans,v[v[i].predecessor[j]].D,4);//update value of D of all predecessor
				
			}
		}
	}		
};

